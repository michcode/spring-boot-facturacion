package com.ymt.datajpa.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "facturas_items")
public class ItemFactura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer cantidad;

    /**
     * Relacionado solo con el producto, ya que con factura es una relacion unidireccional...no necesita de la factura....producto tampoco
     * @JoinColumn(name = "product_id"): No es necesario definirlo
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "producto_id") //se sobre entiende que va crear la llave foranea producto_id en la tabla facturas_items, de forma automatica
    private Producto producto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }


    /**
     * Metodo para calcular importe
     *
     * @return
     */
    public Double calcularImporte() {
        return cantidad.doubleValue() * producto.getPrecio();
    }
}
